<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super1 = new User();
        $super1->name = 'TZ';
        $super1->email = 'tz.sevastyanov@gmail.com';
        $super1->password = Hash::make('tZ1477');
        $super1->id = (string) Str::uuid();
        $super1->save();
    }
}
