<?php

Route::group(['prefix' => 'projects'], function () {
    Route::get('/', 'ProjectController@getAll');
    Route::post('/create', 'ProjectController@create');
    Route::post('/{id}/update', 'ProjectController@update');
    Route::post('/{id}/delete', 'ProjectController@delete');
});

Route::post('/files', 'ApiController@uploadFile');

Route::get('/{vue?}', function () {
    return view('welcome');
})->where('vue', '[\/\w\.-]*');

Auth::routes();

Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');
});
Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});
