import Layout from '../components/public/Layout.vue'
import Home from '../components/public/Home.vue'
import Login from '../components/public/Login.vue'
import Project from '../components/public/ProjectInfo.vue'
import AdminLayout from '../components/admin/Layout.vue'
import AdminHome from '../components/admin/Home.vue'
import AdminProjects from '../components/admin/projects/Projects.vue'
import AdminProjectEdit from '../components/admin/projects/ProjectEdit.vue'

const routes = [{
    path: '/',
    redirect: '/home',
    component: Layout,
    children: [{
        path: '',
        name: 'home',
        component: Home,
    },
        {
            path: 'projects/:id',
            name: 'projectInfo',
            component: Project,
            props: true
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
    ]
},
    {
        path: '/admin',
        redirect: '/admin/home',
        component: AdminLayout,
        meta: {
            auth: true
        },
        children: [
            {
                path: 'home',
                name: 'adminHome',
                component: AdminHome,
            },
            {
                path: 'projects',
                name: 'adminProjects',
                component: AdminProjects
            },
            {
                path: 'projects/:id',
                name: 'adminProjectEdit',
                component: AdminProjectEdit,
                props: true
            },
        ]
    }];

export default routes
