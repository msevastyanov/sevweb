export default class Project {
    constructor (obj) {
        obj = obj || {};
        this.id = obj.id || '';
        this.name = obj.name || '';
        this.year = obj.year || '';
        this.title = obj.title || '';
        this.description = obj.description || '';
        this.cover = obj.description || '';
        this.sort = obj.description || 0;
        this.images = obj.images || [];
    }
}
