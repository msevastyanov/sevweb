import Vue from "vue"
import VueRouter from 'vue-router'
import App from './components/App.vue'
import store from './store'
import routes from './routes/routes'
import VueAxios from 'vue-axios';
import axios from 'axios';

import { APP_URL } from './config.js'

import "./styles/main.scss"

require('./bootstrap');

window.Vue = require('vue');

Vue.use(VueRouter);

Vue.use(VueAxios, axios);

export var router = new VueRouter({
    mode: 'history',
    hashbang: false,
    base: APP_URL,
    routes,
    linkActiveClass: 'active'
});

Vue.router = router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

store.dispatch('checkProjectsSync');

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')