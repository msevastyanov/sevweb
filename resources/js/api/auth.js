import axios from 'axios'
import { APP_URL } from '../config.js'

export class AuthApi {
    async login(creds) {
        let response = await axios.post(`${APP_URL}api/auth/login`, creds)
        return response.data.status === 'success'
    }

    async logout() {
        let response = await axios.post(`${APP_URL}api/auth/logout`)
        return response.data.status === 'success'
    }

    async checkToken() {
        let status
        await axios.get(`${APP_URL}api/auth/checkToken`).then(res => {
            status = true
        }, res => {
            status = false
        })
        return status
    }
}
