import axios from "axios"
import { APP_URL } from '../config.js'

export class ProjectsApi {
    async get() {
        let response = await axios.get(`${APP_URL}projects`);
        return response.data.projects
    }

    async add(project) {
        let response = await axios.post(`${APP_URL}projects/create`, project);
        console.log('response', response)
        return response.data
    }

    async update(project) {
        let response = await axios.post(`${APP_URL}projects/${project.id}/update`, project);
        return response.data
    }

    async delete(id) {
        let response = await axios.post(`${APP_URL}projects/${id}/delete`);
        return response.data
    }
}