import { ProjectsApi } from '../../api/index'
import Project from "../../models/project"

import Vue from 'vue'

const api = new ProjectsApi();

const state = {
    projects: []
};

const getters = {
    getProjectById: state => id => {
        return state.projects.find(p => p.id === id) || new Project()
    }
};

const actions = {
    async getAllProjects ({ commit }) {
        let projects = await api.get();
        commit('setProjects', projects)
    },

    async addProject({ commit }, project) {
        let response = await api.add(project);
        let status = response.status === 'success';

        let createdProject;
        if (status) {
            createdProject = response.content.project;
            commit('addProject', createdProject);
        }

        return {
            status: status,
            content: createdProject
        }
    },

    async updateProject({ commit }, project) {
        let response = await api.update(project);
        console.log('RESP UPD', response)
        let status = response.status === 'success';

        let updatedProject;
        if (status) {
            updatedProject = response.content.project;
            commit('updateProject', updatedProject);
        }

        return {
            status: status,
            content: updatedProject
        }
    },

    async deleteProject({ commit }, id) {
        let response = await api.delete(id);
        let status = response.status === 'success';

        if (status) {
            commit('deleteProject', id)
        }

        return status
    },

    async checkProjectsSync (state) {
        let needSync = !state.projects || !state.projects.length;

        if (needSync) {
            await this.dispatch('getAllProjects')
        }
    }
};

const mutations = {
    setProjects (state, projects) {
        state.projects = projects
    },

    addProject (state, project) {
        state.projects.push(project)
    },

    updateProject (state, project) {
        let index = state.projects.findIndex(p => p.id === project.id);
        Vue.set(state.projects, index, project)
    },

    deleteProject (state, id) {
        state.projects = state.projects.filter(p => p.id !== id)
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
