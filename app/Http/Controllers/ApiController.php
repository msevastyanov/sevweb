<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiController
{
    public function uploadFile(Request $request)
    {
//        $path = Storage::disk('s3')->put('',$request->file('file'), 'public');
        $path = $request->file('file')->store('uploads');

        return response()->json(['status' => 'success', 'content' => $path], 201);
    }
}
