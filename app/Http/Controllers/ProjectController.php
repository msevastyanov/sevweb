<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectRepository;

class ProjectController extends Controller
{
    protected $projects;

    public function __construct(ProjectRepository $repository)
    {
        $this->projects = $repository;
    }

    public function getAll(Request $request)
    {
        $projects = $this->projects->getAll();

        return response()->json(compact('projects'));
    }

    public function create(Request $request)
    {
        $project = $this->projects->create($request);

        return response()->json(['status' => 'success', 'content' => compact('project')], 201);
    }

    public function update(Request $request, $id)
    {
        $project = $this->projects->update($request, $id);

        return response()->json(['status' => 'success', 'content' => compact('project')], 201);
    }

    public function delete($id)
    {
        $this->projects->delete($id);

        return response()->json(['status' => 'success'], 201);
    }
}
