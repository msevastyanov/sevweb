<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Project;
use App\File;
use Illuminate\Support\Str;

class ProjectRepository {
    public function updateProjectFields($isNew, $project, $request) {
        $project->name = $request->name;
        $project->title = $request->title;
        $project->year = $request->year;
        $project->url = $request->url;
        $project->technologies = $request->technologies;
        $project->dev_period = $request->dev_period;
        $project->description = $request->description;
        $project->cover = $request->cover;
        $project->sort = $request->sort;

        $this->updateImages($isNew, $project, $request->images);
    }

    public function updateImages(bool $isNew, $project, $newImages) {
        $project_images = File::where('project_id', $project->id)->get();

        if (!$isNew) {
            foreach ($project_images as $image) {
                if (!in_array($image, $newImages)) {
                    $image->delete();
                }
            }
        }

        foreach ($newImages as $image) {
            if (!in_array($image, $project_images->toArray())) {
                $file = new File;
                $file->id = (string) Str::uuid();
                $file->url = $image;
                $file->project_id = $project->id;

                $file->save();
            }
        }
    }

    public function getProjectImages($project) {
        return File::where('project_id', $project->id)->get()->map(function($image) {
            return $image->url;
        });
    }

    public function getAll() {
        $projects = Project::all();
        return $projects->map(function($project) {
            $project->images = $this->getProjectImages($project);
            return $project;
        });
    }

    public function create($request) {
        $project = new Project;
        $project->id = (string) Str::uuid();

        $this->updateProjectFields(true, $project, $request);

        $project->save();
        $project->images = $this->getProjectImages($project);

        return $project;
    }

    public function update(Request $request, $id) {
        $project = Project::find($id);

        $this->updateProjectFields(false, $project, $request);

        $project->save();
        $project->images = $this->getProjectImages($project);

        return $project;
    }

    public function delete($id) {
        Project::find($id)->delete();
    }

}